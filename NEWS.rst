git-assembler 1.1: 2020-08-03
-----------------------------

* Fixes ``rebase`` behavior, thanks to Richard Nguyen.
* Tests portability fixes, thanks to Carlo Arenas.
* New ``--color`` flag to control terminal coloring.
* Fixes failure to restore starting branch in some cases.
